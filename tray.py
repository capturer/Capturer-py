import os
import sqlite3
import time

import keyboard
from infi.systray import SysTrayIcon
from PIL import ImageGrab
from win10toast import ToastNotifier


def say_hello(systray):
    cap()


def cap():
    conn = sqlite3.connect('sets.db')
    c = conn.cursor()
    filePath = os.path.expanduser('~') + str(time.time()) + '.png'
    try:
        for row in c.execute("SELECT * FROM sets WHERE name = 'path'"):
            filePath = row['content'] + str(time.time()) + '.png'
    except sqlite3.OperationalError:
        c.execute("CREATE TABLE sets (name text, content text)")
        c.execute("INSERT INTO sets VALUES ('path', '" +
                  os.path.expanduser('~') + "')")

    conn.close

    print(filePath)
    ImageGrab.grab().save(filePath, "PNG")
    toaster = ToastNotifier()
    toaster.show_toast("Screenshot captured",
                       f"Screenshot saved as {filePath}",
                       icon_path=os.path.dirname(
                           os.path.realpath(__file__)) + "\\icon.ico",
                       duration=3)


shortcut = 'print screen'
keyboard.add_hotkey(shortcut, cap)

menu_options = (("Take screenshot", None, say_hello),)
systray = SysTrayIcon(os.path.dirname(os.path.realpath(
    __file__)) + "\\icon.ico", "Capturer", menu_options)
systray.start()
keyboard.wait()
